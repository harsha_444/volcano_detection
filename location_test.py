import folium
import pandas
import geopy
from geopy.geocoders import Nominatim
nom=Nominatim()

# Getting the user input location.
x=input("Enter the location:")

# Knowing the concise address of the specified location using nominatim.
location=nom.geocode(x)
# Separating the values of latitude and longitude from the obtained address.
r_latitude=location.latitude
r_longitude=location.longitude

print(r_latitude)
print(r_longitude)
