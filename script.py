import folium
import pandas
import geopy
from geopy.geocoders import Nominatim
nom=Nominatim()

# Getting the user input location.
x=input("Enter the location:")

# Knowing the concise address of the specified location using nominatim.
location=nom.geocode(x)
# Separating the values of latitude and longitude from the obtained address.
r_latitude=location.latitude
r_longitude=location.longitude

def color(elev):
    minimum=int(min(df['ELEV']))
    step=int((max(df['ELEV'])-min(df['ELEV']))/3)
    if elev in range(minimum,minimum+step):
        col='green'
    elif elev in range(minimum+step,minimum+step*2):
        col='orange'
    else:
        col='red'
    return col

# asking for user input range to check volcanoes at that specified range.
"""y=input("Specify the range(in ±degrees):")
y=int(y)

max_lat=r_latitude + y;
min_lat=r_latitude - y;
max_lon=r_longitude + y;
min_lon=r_longitude - y;"""

df=pandas.read_csv("convertcsv.csv",encoding="utf8")

#intially showing the map at location specified with a specific zoom.
map=folium.Map(location=[r_latitude,r_longitude],zoom_start=7,tiles='Stamen Terrain')

for lat,lon,name,elev in zip(df['LAT'],df['LON'],df['NAME'],df['ELEV']):
    map.add_child(folium.Marker(location=[lat,lon],popup=name,icon=folium.Icon(color=color(elev))))


map.save(outfile='vol_map.html')
